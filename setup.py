from setuptools import setup, find_packages

setup(
    name='gray-goose',
    version='0.1.0',
    description='A short description of my package',
    author='Your Name',
    author_email='youremail@example.com',
    url='https://gitlab.com/fire-ball/gray-goose.git',
    package=find_packages(),
)
